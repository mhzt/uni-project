const { DataTypes } = require('sequelize')
const sequelize = require('../sequelize')

const Food = sequelize.define('Food', {
  title: {
    type: DataTypes.STRING
  },
  description: {
    type: DataTypes.STRING
  },
  image: {
    type: DataTypes.STRING
  },
  category: {
    type: DataTypes.STRING,
    defaultValue: 'default'
  },
  featured: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  available: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  }
}, {
  timestamps: true // Enable timestamps (createdAt, updatedAt)
})

module.exports = Food
