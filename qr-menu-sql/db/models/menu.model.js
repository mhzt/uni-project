const { DataTypes } = require('sequelize')
const sequelize = require('../sequelize')

const Menu = sequelize.define('Menu', {
  active: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  }
}, {
  timestamps: true // Enable timestamps (createdAt, updatedAt)
})

module.exports = Menu
