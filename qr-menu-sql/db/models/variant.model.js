const { DataTypes } = require('sequelize')
const sequelize = require('../sequelize')

const Variant = sequelize.define('Variant', {
  title: {
    type: DataTypes.STRING,
    defaultValue: ''
  },
  description: {
    type: DataTypes.STRING,
    defaultValue: ''
  },
  originalPrice: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  price: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  discount: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  available: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  deleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  }
}, {
  timestamps: false
})

module.exports = Variant
