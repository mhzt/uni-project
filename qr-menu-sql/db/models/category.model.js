const { DataTypes } = require('sequelize')
const sequelize = require('../sequelize')

const Category = sequelize.define('Category', {
  title: {
    type: DataTypes.STRING,
    defaultValue: ''
  },
  slug: {
    type: DataTypes.STRING,
    defaultValue: ''
  },
  description: {
    type: DataTypes.STRING,
    defaultValue: ''
  },
  featured: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  active: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  }
})

module.exports = Category
