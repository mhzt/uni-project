const { DataTypes } = require('sequelize')
const sequelize = require('../sequelize')

const Vendor = sequelize.define('Vendor', {
  name: {
    type: DataTypes.STRING,
    defaultValue: ''
  },
  lat: {
    type: DataTypes.FLOAT
  },
  lng: {
    type: DataTypes.FLOAT
  },
  address: {
    type: DataTypes.STRING
  },
  description: {
    type: DataTypes.STRING,
    defaultValue: ''
  },
  active: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  }
}, {
  timestamps: false,
  underscored: true
})

module.exports = Vendor
