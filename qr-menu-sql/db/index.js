const db = require('./sequelize')
const logger = require('../utils/logger.util')('DB')

const Menu = require('./models/menu.model')
const Food = require('./models/food.model')
const Category = require('./models/category.model')
const Vendor = require('./models/vendor.model')
const Variant = require('./models/variant.model')

// Define associations
Menu.hasOne(Vendor, { foreignKey: 'menu_id', as: 'info' })
Menu.hasMany(Category, { foreignKey: 'menu_id', as: 'categories' })
Category.belongsTo(Menu, { foreignKey: 'menu_id', as: 'menu' })
Category.hasMany(Food, { foreignKey: 'category_id', as: 'foods' })
Food.belongsTo(Category, { foreignKey: 'category_id', as: 'food_category' })
Food.hasMany(Variant, { foreignKey: 'food_id', as: 'variants' })
Food.hasMany(Variant, { foreignKey: 'food_id', as: 'extras' })
Variant.belongsTo(Food, { foreignKey: 'food_id', as: 'food' })

// Sync database
db.sync({ force: false })
  .then(() => { logger.log('Database synced') })
  .catch((err) => { logger.error(err) })

Menu.new = async (info) => {
  const menu = await Menu.create({})
  await Vendor.create({ ...info, menu_id: menu.id })
  await Category.create({ title: 'default', slug: 'default', description: 'default', menu_id: menu.id })
  return menu
}

Menu.editInfo = async (id, info) => {
  const menu = await Menu.findByPk(id)
  const vendor = await Vendor.findOne({ where: { menu_id: menu.id } })
  await vendor.update(info)
  return menu
}

Menu.addCategory = async (id, category) => {
  const menu = await Menu.findByPk(+id)
  const newCategory = await Category.create({ ...category, menu_id: menu.id })
  return newCategory
}

Menu.updateCategory = async (id, categoryObj) => {
  const category = Category.findByPk(id)
  await category.update(categoryObj)
  return category
}

Menu.deleteCategory = async (id) => {
  const category = await Category.findByPk(id)
  const foods = await category.getFoods()
  foods.forEach(async (food) => { await Menu.deleteFood(food.id) })

  await category.destroy()
  return category
}

Menu.addFood = async (id, food) => {
  const { variants, extras = [], ...foodObj } = food
  const category = await Category.findByPk(id)
  const newFood = await Food.create({ ...foodObj, category_id: category.id })
  for (const variant of variants) {
    await Variant.create({ ...variant, food_id: newFood.id })
  }

  for (const extra of extras) {
    await Variant.create({ ...extra, food_id: newFood.id })
  }

  return newFood
}

Menu.updateFood = async (id, foodObj) => {
  const food = await Food.findByPk(id)
  await food.update(foodObj)
  return food
}

Menu.deleteFood = async (id) => {
  const food = await Food.findByPk(id)

  const variants = await food.getVariants()
  variants.forEach(async (variant) => { await variant.destroy() })

  const extras = await food.getExtras()
  extras.forEach(async (extra) => { await extra.destroy() })

  await food.destroy()
  return food
}

Menu.changeFoodAvailability = async (variantId, available) => {
  const variant = await Variant.findByPk(variantId)
  await variant.update({ available })
  return variant
}

function deg2rad (deg) {
  return deg * (Math.PI / 180)
}

function getDistanceFromLatLonInKm (lat1, lon1, lat2, lon2) {
  const R = 6371 // Radius of the earth in km
  const dLat = deg2rad(lat2 - lat1) // deg2rad below
  const dLon = deg2rad(lon2 - lon1)
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
  const d = R * c // Distance in km
  return d
}

Menu.nearVendors = async (lat, lng) => {
  // Get title, description and menu_id of vendors in range
  const vendors = await Vendor.findAll({
    attributes: ['title', 'description', 'menu_id', 'lat', 'lng'],
    where: {
      lat: {
        $between: [lat - 0.1, lat + 0.1]
      },
      lng: {
        $between: [lng - 0.1, lng + 0.1]
      }
    }
  })

  // Calculate distance of each vendor from the given lat, lng
  const vendorsWithDistance = vendors.map((vendor) => {
    const distance = getDistanceFromLatLonInKm(lat, lng, vendor.lat, vendor.lng)
    return { ...vendor.toJSON(), distance }
  }).filter((vendor) => vendor.distance < 5)

  // Sort vendors by distance
  vendorsWithDistance.sort((a, b) => a.distance - b.distance)
  return vendorsWithDistance
}

Menu.getMenu = async (id) => {
  const menu = await Menu.findByPk(id, {
    include: [{
      model: Vendor,
      as: 'info'
    }, {
      model: Category,
      as: 'categories',
      include: [{
        model: Food,
        as: 'foods',
        include: [{
          model: Variant,
          as: 'variants'
        }, {
          model: Variant,
          as: 'extras'
        }]
      }]
    }]
  })

  return menu.toJSON()
}

module.exports = {
  Menu,
  Food,
  Category,
  Vendor,
  Variant
}
