const sequelize = require("sequelize")
const logger = require("../utils/logger.util")("DB")
const configs = {
    host: process.env.DB_HOST,
    dialect: "postgres",
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT,
}

const db = new sequelize(configs)

db.authenticate()
    .then(() => logger.log("Connection has been established successfully."))
    .catch((err) => logger.error("Unable to connect to the database:", err))
  
module.exports = db