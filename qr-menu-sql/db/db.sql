BEGIN;


CREATE TABLE IF NOT EXISTS public."Categories"
(
    id integer NOT NULL DEFAULT nextval('"Categories_id_seq"'::regclass),
    title character varying(255) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    slug character varying(255) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    description character varying(255) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    featured boolean DEFAULT false,
    active boolean DEFAULT true,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    menu_id integer,
    CONSTRAINT "Categories_pkey" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."Food"
(
    id integer NOT NULL DEFAULT nextval('"Food_id_seq"'::regclass),
    title character varying(255) COLLATE pg_catalog."default",
    description character varying(255) COLLATE pg_catalog."default",
    image character varying(255) COLLATE pg_catalog."default",
    category character varying(255) COLLATE pg_catalog."default" DEFAULT 'default'::character varying,
    featured boolean DEFAULT false,
    available boolean DEFAULT true,
    deleted boolean DEFAULT false,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    category_id integer,
    CONSTRAINT "Food_pkey" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."Menus"
(
    id integer NOT NULL DEFAULT nextval('"Menus_id_seq"'::regclass),
    active boolean DEFAULT true,
    deleted boolean DEFAULT false,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    CONSTRAINT "Menus_pkey" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public."Variants"
(
    id integer NOT NULL DEFAULT nextval('"Variants_id_seq"'::regclass),
    title character varying(255) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    description character varying(255) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    "originalPrice" integer DEFAULT 0,
    price integer DEFAULT 0,
    discount integer DEFAULT 0,
    available boolean DEFAULT true,
    deleted boolean DEFAULT false,
    food_id integer,
    CONSTRAINT "Variants_pkey" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.vendors
(
    id integer NOT NULL DEFAULT nextval('vendors_id_seq'::regclass),
    name character varying(255) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    lat double precision,
    lng double precision,
    address character varying(255) COLLATE pg_catalog."default",
    description character varying(255) COLLATE pg_catalog."default" DEFAULT ''::character varying,
    active boolean DEFAULT true,
    menu_id integer,
    CONSTRAINT vendors_pkey PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public."Categories"
    ADD CONSTRAINT "Categories_menu_id_fkey" FOREIGN KEY (menu_id)
    REFERENCES public."Menus" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE SET NULL;


ALTER TABLE IF EXISTS public."Food"
    ADD CONSTRAINT "Food_category_id_fkey" FOREIGN KEY (category_id)
    REFERENCES public."Categories" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE SET NULL;


ALTER TABLE IF EXISTS public."Variants"
    ADD CONSTRAINT "Variants_food_id_fkey" FOREIGN KEY (food_id)
    REFERENCES public."Food" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE SET NULL;


ALTER TABLE IF EXISTS public.vendors
    ADD CONSTRAINT vendors_menu_id_fkey FOREIGN KEY (menu_id)
    REFERENCES public."Menus" (id) MATCH SIMPLE
    ON UPDATE CASCADE
    ON DELETE SET NULL;

END;