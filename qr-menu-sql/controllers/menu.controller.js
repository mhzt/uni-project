const { Menu } = require('../db')
const { generateResponse, fetchFromDb, sendSuccess } = require('../utils/response.util')

module.exports.createMenu = async (req, res) => {
  const { info, owner } = req.body
  return generateResponse(res, Menu.new(info, owner), true, Menu)
}

module.exports.editMenuInfo = async (req, res) => {
  return generateResponse(res, Menu.editInfo(req.params.id, req.body), true, Menu)
}

module.exports.addCategory = async (req, res) => {
  const { id } = req.params
  const category = req.body
  generateResponse(res, Menu.addCategory(id, category))
}

module.exports.updateCategory = async (req, res) => {
  const { id, categoryId } = req.params
  const category = req.body
  await fetchFromDb(Menu.findByPk(id), res)
  generateResponse(res, Menu.updateCategory(categoryId, category))
}

module.exports.deleteCategory = async (req, res) => {
  const { id, categoryId } = req.params
  await fetchFromDb(Menu.findByPk(id), res)
  generateResponse(res, Menu.deleteCategory(categoryId))
}

module.exports.addFood = async (req, res) => {
  const { id } = req.params
  const { category, ...food } = req.body
  await fetchFromDb(Menu.findByPk(id), res)
  generateResponse(res, Menu.addFood(category, food))
}

module.exports.updateFood = async (req, res) => {
  const { id, foodId } = req.params
  const food = req.body
  await fetchFromDb(Menu.findByPk(id), res)
  generateResponse(res, Menu.updateFood(foodId, food))
}

module.exports.deleteFood = async (req, res) => {
  const { id, foodId } = req.params
  const menu = await fetchFromDb(Menu.findByPk(id), res)
  generateResponse(res, menu.deleteFood(foodId))
}

module.exports.changeFoodAvailability = async (req, res) => {
  const { foodId } = req.params
  const { available } = req.body
  generateResponse(res, Menu.changeFoodAvailability(foodId, available))
}

module.exports.getMenu = async (req, res) => {
  const { id } = req.params
  generateResponse(res, Menu.getMenu(id), true, Menu)
}
