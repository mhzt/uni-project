const { Menu } = require('../db')
const { generateResponse, sendError } = require('../utils/response.util')

module.exports.nearVendors = async (req, res) => {
  const { lat, lon } = req.query
  if (!lat || !lon) return sendError(res, 400, 'Lat and Lon are required.')
  return generateResponse(res, Menu.nearVendors(lat, lon), false, 'Vendor')
}
