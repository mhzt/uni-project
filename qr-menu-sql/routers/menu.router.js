const express = require('express')
const router = express.Router()

const menuController = require('../controllers/menu.controller')

router.post('/', menuController.createMenu)
router.get('/:id', menuController.getMenu)
router.patch('/:id', menuController.editMenuInfo)

router.post('/:id/categories', menuController.addCategory)
router.patch('/:id/categories/:categoryId', menuController.updateCategory)
router.delete('/:id/categories/:categoryId', menuController.updateCategory)

router.post('/:id/foods', menuController.addFood)
router.patch('/:id/foods/:categoryId', menuController.updateFood)
router.delete('/:id/foods/:categoryId', menuController.deleteFood)
router.post('/:id/foods/:foodId/availability', menuController.changeFoodAvailability)

module.exports = router
