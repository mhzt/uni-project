require('dotenv').config()
const { Menu } = require('../db')

function getLoremIpsum (wordCount = 3) {
  const loremIpsum =
    'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
  return loremIpsum
    .split(' ')
    .sort(() => 0.5 - Math.random())
    .slice(0, wordCount)
    .join(' ')
}

function getRandomLocation (minLat, maxLat, minLon, maxLon) {
  const lat = Math.random() * (maxLat - minLat) + minLat
  const lng = Math.random() * (maxLon - minLon) + minLon
  return { lat, lng }
}

function getVariants (count = 1) {
  return Array(count)
    .fill()
    .map(() => ({
      title: getLoremIpsum(2),
      description: getLoremIpsum(10),
      originalPrice: Math.floor(Math.random() * 1000),
      discount: Math.floor(Math.random() * 100),
      available: Math.random() > 0.5
    }))
}

function getRandomFood (category) {
  return {
    title: getLoremIpsum(2),
    description: getLoremIpsum(10),
    image: 'https://picsum.photos/200',
    category,
    variants: getVariants(Math.floor(Math.random() * 3) + 1),
    featured: Math.random() > 0.5,
    available: Math.random() > 0.5
  }
}

function getRandomCategory () {
  return {
    title: getLoremIpsum(2),
    slug: Math.floor(Math.random() * 100).toString() + '-' + getLoremIpsum(1),
    description: getLoremIpsum(10),
    image: 'https://picsum.photos/200',
    available: Math.random() > 0.5
  }
}

function getRandomVendorInfo () {
  return {
    name: getLoremIpsum(2),
    description: getLoremIpsum(10),
    image: 'https://picsum.photos/200',
    ...getRandomLocation(1, 10, 1, 10)
  }
}

async function createMenu () {
  const info = getRandomVendorInfo()
  const newMenu = await Menu.new(info)

  for (let i = 0; i < Math.random() * 10; i++) {
    const category = await Menu.addCategory(newMenu.id, getRandomCategory())

    for (let i = 0; i < Math.random() * 10; i++) {
      const food = getRandomFood(category.id)
      await Menu.addFood(category.id, food)
    }
  }
}

async function main () {
  for (let i = 0; i < 900; i++) {
    await createMenu()
    console.log('Menu created', i)
  }
  process.exit(0)
}

main()
