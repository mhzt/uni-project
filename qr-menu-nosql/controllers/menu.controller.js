const { Menu } = require('../models')
const { generateResponse, fetchFromDb, sendSuccess } = require('../utils/response.util')

module.exports.createMenu = async (req, res) => {
  const { info, owner } = req.body
  return generateResponse(res, Menu.new(info, owner), true, Menu)
}

module.exports.editMenuInfo = async (req, res) => {
  return generateResponse(res, Menu.editInfo(req.params.id, req.body), true, Menu)
}

module.exports.addCategory = async (req, res) => {
  const { id } = req.params
  const category = req.body
  const menu = await fetchFromDb(Menu.findById(id), res)
  generateResponse(res, menu.addCategory(category))
}

module.exports.getCategories = async (req, res) => {
  const { id } = req.params
  const menu = await fetchFromDb(Menu.findById(id), res)
  sendSuccess(res, menu.toJSON().allCategories)
}

module.exports.updateCategory = async (req, res) => {
  const { id, categoryId } = req.params
  const category = req.body
  const menu = await fetchFromDb(Menu.findById(id), res)
  generateResponse(res, menu.updateCategory(categoryId, category))
}

module.exports.deleteCategory = async (req, res) => {
  const { id, categoryId } = req.params
  const menu = await fetchFromDb(Menu.findById(id), res)
  generateResponse(res, menu.deleteCategory(categoryId))
}

module.exports.addFood = async (req, res) => {
  const { id } = req.params
  const food = req.body
  const menu = await fetchFromDb(Menu.findById(id), res)
  generateResponse(res, menu.addFood(food))
}

module.exports.updateFood = async (req, res) => {
  const { id, foodId } = req.params
  const food = req.body
  const menu = await fetchFromDb(Menu.findById(id), res)
  generateResponse(res, menu.updateFood(foodId, food))
}

module.exports.deleteFood = async (req, res) => {
  const { id, foodId } = req.params
  const menu = await fetchFromDb(Menu.findById(id), res)
  generateResponse(res, menu.deleteFood(foodId))
}

module.exports.changeFoodAvailability = async (req, res) => {
  const { id, foodId } = req.params
  const { available } = req.body
  generateResponse(res, Menu.changeFoodAvailability(id, foodId, available))
}
