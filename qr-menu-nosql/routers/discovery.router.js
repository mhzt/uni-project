const express = require('express')
const router = express.Router()

const discoveryController = require('../controllers/discovery.controller')

router.get('/', discoveryController.nearVendors)

module.exports = router
