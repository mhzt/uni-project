const express = require('express')
const router = express.Router()

const { Menu } = require('../models')

const { crud } = require('../controllers')
const menuController = require('../controllers/menu.controller')

const crudController = crud(Menu, 'Menu', ['owner'])

router.get('/', crudController.getAll)
router.post('/', menuController.createMenu)
router.get('/:id', crudController.get)
router.patch('/:id', menuController.editMenuInfo)
router.delete('/:id', crudController.softDelete)

router.get('/:id/categories', menuController.getCategories)
router.post('/:id/categories', menuController.addCategory)
router.patch('/:id/categories/:categoryId', menuController.updateCategory)
router.delete('/:id/categories/:categoryId', menuController.updateCategory)

router.post('/:id/foods', menuController.addFood)
router.patch('/:id/foods/:categoryId', menuController.updateFood)
router.delete('/:id/foods/:categoryId', menuController.deleteFood)
router.post('/:id/foods/:foodId/availability', menuController.changeFoodAvailability)

module.exports = router
