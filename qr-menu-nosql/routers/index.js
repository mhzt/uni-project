const express = require('express')
const router = express.Router()

const menuRouter = require('./menu.router')
const discoveryRouter = require('./discovery.router')

// Health check route
router.get('/', (_, res) => res.json({ time: Date.now() }))

// APIs
router.use('/menu', menuRouter)
router.use('/discovery', discoveryRouter)

module.exports = router
