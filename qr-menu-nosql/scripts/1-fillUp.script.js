require('dotenv').config()
const { Menu } = require('../models')

function getLoremIpsum (wordCount = 3) {
  const loremIpsum = 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
  return loremIpsum.split(' ').sort(() => 0.5 - Math.random()).slice(0, wordCount).join(' ')
}

function getRandomLocation (minLat, maxLat, minLon, maxLon) {
  const lat = (Math.random() * (maxLat - minLat)) + minLat
  const lon = (Math.random() * (maxLon - minLon)) + minLon
  return { type: 'Point', coordinates: [lat, lon] }
}

function getVariants (count = 1) {
  return Array(count).fill().map(() => ({
    title: getLoremIpsum(2),
    description: getLoremIpsum(10),
    originalPrice: Math.floor(Math.random() * 1000),
    discount: Math.floor(Math.random() * 100),
    available: Math.random() > 0.5
  }))
}

function getRandomFood (category) {
  return {
    title: getLoremIpsum(2),
    description: getLoremIpsum(10),
    image: 'https://picsum.photos/200',
    category,
    variants: getVariants(Math.floor(Math.random() * 3) + 1),
    featured: Math.random() > 0.5,
    available: Math.random() > 0.5
  }
}

function getRandomCategory (i) {
  return {
    title: getLoremIpsum(2),
    description: getLoremIpsum(10),
    image: 'https://picsum.photos/200',
    available: Math.random() > 0.5
  }
}

function getRandomVendorInfo () {
  return {
    name: getLoremIpsum(2),
    description: getLoremIpsum(10),
    image: 'https://picsum.photos/200',
    location: getRandomLocation(1, 10, 1, 10)
  }
}

let created = 1

async function createMenu (idx) {
  const newMenu = new Menu({
    info: getRandomVendorInfo()
  })

  await newMenu.save()

  for (let i = 0; i < Math.random() * 10; i++) {
    const category = getRandomCategory(idx)
    newMenu.categories.push(category)
  }

  await newMenu.save()

  newMenu.categories.forEach(category => {
    for (let i = 0; i < Math.random() * 10; i++) {
      const food = getRandomFood(category._id.toString())
      newMenu.foods.push(food)
    }
  })

  await newMenu.save()
  created++

  if (created >= idx) {
    process.exit(0)
  }
}

for (let i = 0; i < 1000; i++) {
  createMenu(1000)
}
