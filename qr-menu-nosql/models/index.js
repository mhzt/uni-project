const mongoose = require('mongoose')
const logger = require('../utils/logger.util')('MDB')

mongoose.Promise = global.Promise

mongoose.connect(process.env.MONGO || 'mongodb://localhost:27017/myapp', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

mongoose.connection.on('connected', () => {
  logger.log('Mongoose connected to ' + process.env.MONGO)
})

mongoose.connection.on('error', (err) => {
  logger.error('Mongoose connection error: ' + err)
})

mongoose.connection.on('disconnected', () => {
  logger.log('Mongoose disconnected')
})

const { Menu } = require('./schemas/menu.schema')
module.exports.Menu = Menu
