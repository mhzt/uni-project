const { Schema, model } = require('mongoose')
const { VendorSchema } = require('./vendor.subschema')
const { FoodSchema } = require('./food.subschema')
const { CategorySchema } = require('./category.subschema')

const MenuSchema = new Schema({
  info: { type: VendorSchema, required: true },
  foods: { type: [FoodSchema], default: [] },
  categories: {
    type: [CategorySchema],
    default: [{
      title: 'Default',
      slug: 'default',
      description: 'Default category',
      featured: false,
      active: true
    }]
  },
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  active: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false }
}, { timestamps: true })

MenuSchema.pre('save', function (next) {
  // check food categories
  const categories = this.categories.map(category => category.id)
  const categoriesAreValid = this.foods.every(food => categories.includes(food.category))
  if (!categoriesAreValid) { return next(new Error('Invalid food categories')) }

  // check category slugs are unique
  const slugs = this.categories.map(category => category.slug)
  const slugsAreUnique = slugs.length === new Set(slugs).size
  if (!slugsAreUnique) { return next(new Error('Category slugs must be unique')) }

  return next()
})

MenuSchema.set('toJSON', {
  transform: (_doc, ret) => {
    ret.id = ret._id
    delete ret._id
    delete ret.__v

    ret.categories = ret.categories || []
    ret.foods = ret.foods || []
    ret.categories.forEach(category => {
      category.id = category._id
      delete category._id
      delete category.__v
    })

    ret.allCategories = ret.categories

    ret.foods.forEach(food => {
      food.id = food._id
      delete food._id
      delete food.__v
    })

    const activeCategories = ret.categories.filter(category => category.active)
    const categories = activeCategories
      .map(category => {
        const foods = ret.foods.filter(food => food.category.toString() === category.id.toString() && !food.deleted).sort((a, b) => b.featured - a.featured)
        return { ...category, foods }
      })

    ret.categories = categories.filter(category => category.foods.length > 0)
    ret.categories.sort((a, b) => b.featured - a.featured)

    delete ret.foods
    delete ret.owner
  }
})

MenuSchema.methods.addCategory = function (category) {
  this.categories.push(category)
  return this.save()
}

MenuSchema.methods.updateCategory = function (categoryId, category) {
  const index = this.categories.findIndex(category => category.id === categoryId)
  if (index === -1) { throw new Error('Category not found') }

  if (this.categories[index].slug === 'default') {
    throw new Error("Can't edit default category.")
  }

  this.categories[index] = { ...this.categories[index], ...category }
  return this.save()
}

MenuSchema.methods.deleteCategory = function (categoryId) {
  const index = this.categories.findIndex(category => category.id === categoryId)
  if (index === -1) { throw new Error('Category not found') }

  if (this.categories[index].slug === 'default') {
    throw new Error("Can't remove default category.")
  }

  this.categories.splice(index, 1)
  return this.save()
}

MenuSchema.methods.addFood = function (food) {
  this.foods.push(food)
  return this.save()
}

MenuSchema.methods.updateFood = function (foodId, food) {
  const index = this.foods.findIndex(food => food.id === foodId)
  if (index === -1) { throw new Error('Food not found') }

  this.foods[index] = { ...this.foods[index], ...food }
  return this.save()
}

MenuSchema.methods.deleteFood = function (foodId) {
  const index = this.foods.findIndex(food => food.id === foodId)
  if (index === -1) { throw new Error('Food not found') }

  this.foods[index].deleted = true
  return this.save()
}

MenuSchema.methods.editInfo = function (info) {
  this.info = { ...this.info, ...info }
  return this.save()
}

MenuSchema.statics.new = function (info, owner) {
  const menu = new this({ info, owner })
  return menu.save()
}

MenuSchema.statics.editInfo = function (menuId, info) {
  const menu = this.findById(menuId)
  if (!menu) { throw new Error('Menu not found') }
  return menu.editInfo(info)
}

MenuSchema.statics.nearVendors = function (lat, lon) {
  return this.aggregate([
    {
      $geoNear: {
        near: {
          type: 'Point',
          coordinates: [parseFloat(lon), parseFloat(lat)]
        },
        maxDistance: 5000,
        spherical: true,
        distanceField: 'distance'
      }
    },
    {
      $project: {
        id: '$_id',
        _id: 0,
        info: 1 // Include the "info" field
      }
    }
  ])
}

MenuSchema.statics.changeFoodAvailability = function (menuId, variantId, available) {
  return this.updateOne(
    { _id: menuId, 'foods.variants._id': variantId },
    { $set: { 'foods.$.variants.$[variant].available': available } },
    { arrayFilters: [{ 'variant._id': variantId }] }
  )
}

const Menu = model('Menu', MenuSchema)

module.exports = { MenuSchema, Menu }
