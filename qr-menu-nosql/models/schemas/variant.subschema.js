const { Schema } = require('mongoose')

const VariantSchema = new Schema({
  title: { type: String, default: '' },
  description: { type: String, default: '' },
  originalPrice: { type: Number, default: 0 },
  price: { type: Number, default: 0 },
  discount: { type: Number, default: 0 },
  available: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false }
})

VariantSchema.pre('save', function (next) {
  this.price = parseInt(this.originalPrice * (1 - this.discount / 100))
  return next()
})

exports.VariantSchema = VariantSchema
