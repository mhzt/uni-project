const { Schema } = require('mongoose')

const CategorySchema = new Schema({
  title: { type: String, default: '' },
  slug: { type: String, default: () => Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) },
  description: { type: String, default: '' },
  featured: { type: Boolean, default: false },
  active: { type: Boolean, default: true }
})

exports.CategorySchema = CategorySchema
