const { Schema } = require('mongoose')

const VendorSchema = new Schema({
  name: { type: String, default: '' },
  location: {
    type: { type: String, default: 'Point' },
    coordinates: [Number]
  },
  address: String,
  description: { type: String, default: '' },
  active: { type: Boolean, default: true }
}, { _id: false })

VendorSchema.index({ location: '2dsphere' })

exports.VendorSchema = VendorSchema
