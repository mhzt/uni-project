const { Schema } = require('mongoose')
const { VariantSchema } = require('./variant.subschema')

const FoodSchema = new Schema({
  title: { type: String },
  description: { type: String },
  image: { type: String },
  category: { type: String, default: 'default' },
  variants: { type: [VariantSchema], default: [] },
  extras: { type: [VariantSchema], default: [] },
  featured: { type: Boolean, default: false },
  available: { type: Boolean, default: true },
  deleted: { type: Boolean, default: false }
}, { timestamps: true })

FoodSchema.set('toJSON', {
  transform: function (_doc, ret) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v

    ret.available = ret.variants.some(variant => variant.available)
    ret.hasDiscount = ret.variants.some(variant => variant.discount > 0)
    ret.hasExtras = ret.extras.length > 0
    ret.hasVariants = ret.variants.length > 1

    if (ret.variants[0]) {
      ret.title = ret.title || ret.variants[0].title
      ret.description = ret.description || ret.variants[0].description
    }

    ret.variants.forEach(variant => {
      variant.id = variant._id
      delete variant._id
      delete variant.__v
    })

    ret.extras.forEach(extra => {
      extra.id = extra._id
      delete extra._id
      delete extra.__v
    })

    if (ret.variants.length === 1) {
      ret.price = ret.variants[0].price
      ret.originalPrice = ret.variants[0].originalPrice
      ret.discount = ret.variants[0].discount
    }
  }
})

VariantSchema.pre('save', function (next) {
  this.price = parseInt(this.originalPrice * (1 - this.discount / 100))
  return next()
})

exports.FoodSchema = FoodSchema
