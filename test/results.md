# Test Results
## Project specifications

- Node.JS: v16.20.1
- express: v4.18.2
- mongoose: 7.3.4
- sequelize; 6.32.1
- mongodb: v6
- postgresql: v14.8

## Server specifications
### Databases

- RAM: 0.5 GB
- CPU: 0.5 Core
- Disk: 5GB SSD
- Location: Liara-Iran

### API Server

- RAM: 1 GB
- CPU: 1 Core
- Disk: 10GB SSD
- Location: Liara-Iran
- OS: Linux (official docker image)

### Test Runner

- RAM: 4 GB
- CPU: 2 Core
- Disk: 50GB SSD
- Location: Iran-Iran Server
- OS: Ubuntu 22.04

## Test Results
### Retrieve Menu

|             	| Postgresql 	| MongoDB 	|
|--------------	|------------	|---------	|
| Failed       	| 18         	| 6       	|
| Passed       	| 9982       	| 9994    	|
| Total        	| 10000      	| 10000   	|
| Average Time 	| 1698.7     	| 536.2   	|


### Retrieve Near Me

|             	| Postgresql 	| MongoDB 	|
|--------------	|------------	|---------	|
| Failed       	| 12         	| 9       	|
| Passed       	| 9988       	| 9991    	|
| Total        	| 10000      	| 10000   	|
| Average Time 	| 2165.3     	| 742.1   	|

### Update Food Availability
|             	| Postgresql 	| MongoDB 	|
|--------------	|------------	|---------	|
| Failed       	| 0         	| 0       	|
| Passed       	| 10000       | 10000    	|
| Total        	| 10000      	| 10000   	|
| Average Time 	| 428.7     	| 686.2   	|