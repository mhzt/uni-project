const axios = require("axios");
const ids = require("./ids.json");

function getRandomId(max = 1000) {
  return ids[Math.floor(Math.random() * Math.floor(max))];
}

const results = {
  failed: 0,
  passed: 0,
  total: 0,
  responseTimes: [],
};

const testCount = 100;
const testConcurrency = 10;
const baseUrl = "https://uni-project-2.iran.liara.run/api/menu/";

const test = async () => {
  const start = Date.now();
  const id = getRandomId();
  try {
    const response = await axios.get(baseUrl + id);
    const end = Date.now();
    const responseTime = end - start;
    results.responseTimes.push(responseTime);
    if (response.status === 200) {
      results.passed++;
    } else {
      results.failed++;
    }
  } catch (e) {
    results.failed++;
  }
  results.total++;
  console.log(results.total);
};

const runTests = async () => {
  const promises = [];
  for (let i = 0; i < testConcurrency; i++) {
    promises.push(test());
  }
  await Promise.all(promises);
  if (results.total < testCount) {
    await runTests();
  }
};

runTests().then(() => {
  results.responseTimes = results.responseTimes.reduce((a, b) => a + b, 0) / results.responseTimes.length
  console.table(results)
  process.exit(0);
});
